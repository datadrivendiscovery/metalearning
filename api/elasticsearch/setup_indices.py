import elasticsearch
import json
import os
from api.v1.utils.metadata import *

# Currently runs on a cluster hosted at localhost:9200
es = elasticsearch.Elasticsearch()


def attach_alias(alias, index_pattern):
    es.indices.put_alias(index=index_pattern, name=alias)


def create_index(index_name, mapping_file):
    mappings = json.load(open(mapping_file, 'r'))

    # ignore 400 cause by IndexAlreadyExistsException when creating an index
    es.indices.create(index=index_name, body=mappings, ignore=400)


def main():

    pipeline_mapping_file = os.path.join("mappings", "pipelines.json")
    pipeline_run_mapping_file = os.path.join("mappings", "pipeline_runs.json")
    datasets_mapping_file = os.path.join("mappings", "datasets.json")
    problems_mapping_file = os.path.join("mappings", "problems.json")
    primitives_mapping_file = os.path.join("mappings", "primitives.json")

    create_index(PIPELINES_INDEX, pipeline_mapping_file)
    create_index(PIPELINE_RUNS_INDEX, pipeline_run_mapping_file)
    create_index(DATASET_INDEX, datasets_mapping_file)
    create_index(PROBLEMS_INDEX, problems_mapping_file)
    create_index(PRIMITIVES_INDEX, primitives_mapping_file)

    attach_alias(alias=DATASET_ALIAS, index_pattern=DATASET_INDEX)
    attach_alias(alias=PROBLEMS_ALIAS, index_pattern=PROBLEMS_INDEX)
    attach_alias(alias=PRIMITIVES_ALIAS, index_pattern=PRIMITIVES_INDEX)
    attach_alias(alias=PIPELINES_ALIAS, index_pattern=PIPELINES_INDEX)
    attach_alias(alias=PIPELINE_RUNS_ALIAS, index_pattern=PIPELINE_RUNS_INDEX)


if __name__ == '__main__':
    main()
