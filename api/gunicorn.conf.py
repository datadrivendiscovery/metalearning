import multiprocessing

bind = "0.0.0.0:5000"
# https://docs.gunicorn.org/en/stable/configure.html#configuration-file
workers = multiprocessing.cpu_count()
logconfig = "logging.conf"
