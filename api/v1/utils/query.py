import elasticsearch
import logging
from api.v1.utils import metadata
import json
import datetime
import hashlib
from api.api_conf import ElasticsearchConfig

logger = logging.getLogger(__name__)

es_config = ElasticsearchConfig()
# Set global timeout https://elasticsearch-py.readthedocs.io/en/master/api.html#timeout
es = elasticsearch.Elasticsearch(hosts=[{"host": es_config.get_host(), "port": es_config.get_port()}],
                                 timeout=es_config.get_timeout())
# https://elasticsearch-py.readthedocs.io/en/master/#logging
es_logger = logging.getLogger('elasticsearch')
es_logger.setLevel(logging.INFO)


def get_dataset_docs():
    """
    Gets 10 data docs from ES database.
    To get more, use ES directly.
    :return:
    """
    return get_docs_from_index(index=metadata.DATASET_ALIAS)


def get_pipeline_docs():
    """
    Gets 10 pipeline docs from ES database.
    To get more, use ES directly.
    :return:
    """
    return get_docs_from_index(index=metadata.PIPELINES_INDEX)


def get_problem_docs():
    """
    Gets 10 problem docs from ES database.
    To get more, use ES directly.
    :return:
    """
    return get_docs_from_index(index=metadata.PROBLEMS_ALIAS)


def get_pipeline_run_docs():
    """
    Gets 10 pipeline run docs from ES database.
    To get more, use ES directly.
    :return:
    """
    return get_docs_from_index(index=metadata.PIPELINE_RUNS_ALIAS)


def ingest_primitive_doc(doc):
    """
    Ingests primitive doc in the database
    :param doc:
    :return:
    """
    assure_document_does_not_exist(metadata.PRIMITIVES_ALIAS, {"id": doc["id"], "digest": doc["digest"],
                                                               "primitive_code.interfaces_version":
                                                                   doc["primitive_code"]["interfaces_version"]})

    # Primitive docs require a special database id because:
    # * The primitive id does not need to change across versions
    # * The digest is computed only from the installation section and not the entire document
    # * So to have the same primitive be installed in multiple interface versions, we include the interface version
    # in computing the primitive id as well.

    doc_id = "{id}_{digest}_{interfaces_version}".format(id=doc["id"], digest=doc["digest"],
                                                         interfaces_version=doc["primitive_code"]["interfaces_version"])
    logger.debug(f"Ingesting primitive doc: {doc['id']}")
    return ingest_doc(doc=doc, index=metadata.PRIMITIVES_INDEX, doc_type="_doc",
                      id=hashlib.sha256(doc_id.encode('utf-8')).hexdigest())


def ingest_dataset_doc(doc):
    """
    Ingests data doc in the pipelines index
    :param doc:
    :return:
    """
    # We have to make sure across all indices in an alias that the document
    # does not exist. There is no race condition between checking and inserting
    # here because inserting makes sure there are no duplicates for the current
    # index into which it is inserting.
    assure_document_does_not_exist(metadata.DATASET_ALIAS, {"id": doc["id"], "digest": doc["digest"]})

    # Digest of the dataset is over the original underlying data and metadata, and not
    # over the document. So we need to compute an new id for the database for uniqueness
    doc_id = "{digest}_{id}".format(digest=doc["digest"], id=doc["id"])
    logger.debug(f"Ingesting dataset doc: {doc['id']}")
    return ingest_doc(doc=doc, index=metadata.DATASET_INDEX, doc_type="_doc",
                      id=hashlib.sha256(doc_id.encode('utf-8')).hexdigest())


def ingest_pipeline_doc(doc):
    """
    Ingests pipeline doc in the pipelines index
    :param doc:
    :return:
    """
    # We have to make sure across all indices in an alias that the document
    # does not exist. There is no race condition between checking and inserting
    # here because inserting makes sure there are no duplicates for the current
    # index into which it is inserting.
    assure_document_does_not_exist(metadata.PIPELINES_ALIAS, {"id": doc["id"], "digest": doc["digest"]})
    logger.debug(f"Ingesting pipeline doc: {doc['id']}")
    return ingest_doc(doc=doc, index=metadata.PIPELINES_INDEX, doc_type="_doc", id=doc["digest"])


def ingest_pipeline_run_doc(doc, index=metadata.PIPELINE_RUNS_INDEX):
    """
    Ingests pipeline run doc in the pipelines index
    :param doc:
    :return:
    """
    # We have to make sure across all indices in an alias that the document
    # does not exist. There is no race condition between checking and inserting
    # here because inserting makes sure there are no duplicates for the current
    # index into which it is inserting.
    assure_document_does_not_exist(metadata.PIPELINE_RUNS_ALIAS, {"id": doc["id"]})
    logger.debug(f"Ingesting pipeline run doc: {doc['id']}")
    return ingest_doc(doc=doc, index=index, doc_type="_doc", id=doc["id"])


def ingest_problem_doc(doc):
    """
    Ingests pipeline doc in the pipelines index
    :param doc:
    :return:
    """
    # We have to make sure across all indices in an alias that the document
    # does not exist. There is no race condition between checking and inserting
    # here because inserting makes sure there are no duplicates for the current
    # index into which it is inserting.
    assure_document_does_not_exist(metadata.PROBLEMS_ALIAS, {"id": doc["id"], "digest": doc["digest"]})
    logger.debug(f"Ingesting problem doc: {doc['id']}")
    return ingest_doc(doc=doc, index=metadata.PROBLEMS_INDEX, doc_type="_doc", id=doc["digest"])


def ingest_doc(doc, index, doc_type, id):
    """
    Ingests doc in given index
    :param doc:
    :param index:
    :return:
    """
    try:
        resp = es.create(index=index, doc_type=doc_type, body=doc, id=id, refresh='wait_for')
    except Exception as e:
        logger.exception("Could not ingest doc to index %(index)s", {'index': index})
        raise e
    return resp


def get_docs_from_index(index, size=10):
    """
    Gets docs from a given index
    :param index:
    :return:
    """
    return es.search(index=index, size=size)


def assure_document_does_not_exist(index, fields: dict):
    """
    Throws an exception if a duplicate document is found.
    Every endpoint ingesting any document to the database uses this function to make sure a duplicate document does not
    exists.
    We need to do our custom de-duplication and not reply on the unique database ids because we use multiple indices in
    Elasticsearch and they are all aliased. So a unique id in one index is not unique across all indices within the same
    alias.
    Hence, we perform custom de-duplication.
    :param index: Collection to check for duplicate
    :param doc: Document to be ingested
    :return:
    """
    try:
        # If document exists raise error
        assure_document_exists(index=index, **fields)
        raise DuplicateDocumentError
    except DocumentNotFoundError:
        pass


def assure_document_exists(index, **fields):
    """
    Check if a given id exists in the index
    :param index:
    :param fields: Key value pair of term/value combinations to query on
    :return: If it exists
    """

    try:
        must_match = []
        for key, value in fields.items():
            must_match.append({"term": {key: value}})
        query = {
            "query": {
                "bool": {
                    "must": must_match
                }
            },
            "_source": False
        }
        result = es.search(index=index, body=query, doc_type="_doc")
        match_count = result.get("hits", {}).get("total", {}).get("value", 0)
        if match_count == 1:
            return
        elif match_count > 1:
            inconsistency_err_msg = "Referenced document {} occurs {} times in collection: {}.".format(
                json.dumps(fields), match_count, index
            )
            logger.exception(inconsistency_err_msg)
            raise DatabaseInconsistencyError(inconsistency_err_msg + " Please report this.")
        else:
            raise DocumentNotFoundError("Referenced document {} not found in collection: {}.".format(
                json.dumps(fields), index
            ))
    except elasticsearch.NotFoundError as nfe:
        logger.exception("Got NotFoundError while checking for existence of document %(fields)s in index %(index)s",
                         {"fields": json.dumps(fields), "index": index})
        raise nfe
    except Exception as e:
        logger.exception("Error checking for existence of document %(fields)s in index %(index)s",
                         {"fields": json.dumps(fields), "index": index})
        raise e


def add_timestamp(req):
    req['_ingest_timestamp'] = datetime.datetime.utcnow().isoformat() + 'Z'
    return req


def add_submitter(req, submitter):
    req[metadata.SUBMITTER_FIELD] = submitter
    return req


def drop_underscored_fields(req):
    for key in list(req.keys()):
        if key[0] == "_":
            del req[key]
    return req


def get_time_delta(then: datetime):
    return datetime.datetime.now() - then


class DatabaseInconsistencyError(Exception):
    pass


class DocumentNotFoundError(Exception):
    pass


class DuplicateDocumentError(Exception):
    pass
