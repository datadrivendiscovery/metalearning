import uuid
from d3m.metadata import pipeline_run as d3m_pipeline_run
from api.v1.utils.metadata import *
from api.v1.utils import query, validator
import csv
import datetime
import hashlib
import logging
from api.api_conf import TokenConfig

PIPELINE_RUN_NAMESPACE = uuid.UUID('91ebbb2b-4067-456f-9dc4-9de22cdb0a90')
logger = logging.getLogger(__name__)


def validate_primitive_doc(primitive: dict):
    # Validate primitive
    d3m_pipeline_run.validate_primitive(primitive)


def validate_problem_doc(problem: dict):
    time_start = datetime.datetime.now()
    action = f"Validating problem doc: {problem.get('id')}"
    logger.debug(action)
    # Validate problem
    d3m_pipeline_run.validate_problem(problem)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    time_start = datetime.datetime.now()
    action = f"Verifying problem refs for problem doc: {problem.get('id')}"
    logger.debug(action)
    # Verify redacted problem reference
    # https://metadata.datadrivendiscovery.org/devel/?problem#Problem_description.source.from.oneOf.1.problem
    problem_ref = problem.get("source", {}).get("from", {}).get("problem")
    if problem_ref:
        verify_problem_reference_exists(problem_ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")


def validate_dataset_doc(dataset: dict):
    time_start = datetime.datetime.now()
    action = f"Validating dataset doc: {dataset.get('id')}"
    logger.debug(action)
    # Validate dataset
    d3m_pipeline_run.validate_dataset(dataset)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    time_start = datetime.datetime.now()
    action = f"Verifying dataset refs for dataset doc: {dataset.get('id')}"
    logger.debug(action)
    # Verify redacted dataset reference
    # https://metadata.datadrivendiscovery.org/devel/?container#Container_metadata.source.from.oneOf.0.dataset
    dataset_ref = dataset.get("source", {}).get("from", {}).get("dataset")
    if dataset_ref:
        verify_dataset_reference_exists(dataset_ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")


def validate_pipeline_doc(pipeline: dict):
    time_start = datetime.datetime.now()
    action = f"Validating pipeline doc: {pipeline.get('id')}"
    logger.debug(action)
    # Validate pipeline
    d3m_pipeline_run.validate_pipeline(pipeline)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    time_start = datetime.datetime.now()
    action = f"Verifying primitive refs for pipeline doc: {pipeline.get('id')}"
    logger.debug(action)
    # Verify primitive refs exists
    # https://metadata.datadrivendiscovery.org/devel/?pipeline#Pipeline_description.steps
    for step in pipeline.get("steps", []):
        primitive_ref = step.get("primitive")
        if primitive_ref:
            verify_primitive_reference_exists(primitive_ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    time_start = datetime.datetime.now()
    action = f"Verifying pipeline refs for pipeline doc: {pipeline.get('id')}"
    logger.debug(action)
    # Verify pipeline references
    # 1 - https://metadata.datadrivendiscovery.org/devel/?pipeline#Pipeline_description.source.from.oneOf.2.pipelines
    for pipeline_ref in pipeline.get("source", {}).get("from", {}).get("pipelines", []):
        verify_pipeline_reference_exists(pipeline_ref)

    # 2 - https://metadata.datadrivendiscovery.org/devel/?pipeline#Pipeline_description.steps.items.oneOf.1.pipeline
    for step in pipeline.get("steps", []):
        pipeline_ref = step.get("pipeline")
        if pipeline_ref:
            verify_pipeline_reference_exists(pipeline_ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")


def validate_pipeline_run_doc(pipeline_run: dict):
    """
    Validates pipeline run document. Throws a Runtime error if validation fails.
    :param pipeline_run:
    :return:
    """
    time_start = datetime.datetime.now()
    action = f"Validating pipeline run doc: {pipeline_run.get('id')}"
    logger.debug(action)
    # Validate schema
    d3m_pipeline_run.validate_pipeline_run(pipeline_run)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    # Validate referenced docs
    verify_referenced_docs_exist(pipeline_run)


def verify_referenced_docs_exist(pipeline_run: dict):
    """
    Verifies that the dataset and pipeline doc referenced in the pipeline-run exist in the database.
    :param pipeline_run:
    :return:
    """
    time_start = datetime.datetime.now()
    action = f"Verifying dataset refs for pipeline run doc: {pipeline_run.get('id')}"
    logger.debug(action)
    # Verify dataset reference
    # 1 https://metadata.datadrivendiscovery.org/devel/?pipeline_run#Pipeline_run_description.datasets
    for dataset_ref in pipeline_run.get(DATASETS_KEY, []):
        verify_dataset_reference_exists(dataset_ref)

    # 2 - https://metadata.datadrivendiscovery.org/devel/?pipeline_run#Pipeline_run_description.run.scoring.allOf.1.datasets
    for dataset_ref in pipeline_run.get("scoring", {}).get("datasets", []):
        verify_dataset_reference_exists(dataset_ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    time_start = datetime.datetime.now()
    action = f"Verifying problem refs for pipeline run doc: {pipeline_run.get('id')}"
    logger.debug(action)
    # Verify problem doc
    # 3 https://metadata.datadrivendiscovery.org/devel/?pipeline_run#Pipeline_run_description.problem
    problem_ref = pipeline_run.get(PROBLEM_KEY, {})
    if problem_ref:
        verify_problem_reference_exists(problem_ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    time_start = datetime.datetime.now()
    action = f"Verifying pipeline refs for pipeline run doc: {pipeline_run.get('id')}"
    logger.debug(action)
    # Verify pipeline references
    # 4 - https://metadata.datadrivendiscovery.org/devel/?pipeline_run#Pipeline_run_description.pipeline
    pipeline_ref = pipeline_run.get(PIPELINE_KEY, {})
    verify_pipeline_reference_exists(pipeline_ref)

    # 5 - https://metadata.datadrivendiscovery.org/devel/?pipeline_run#Pipeline_run_description.run.data_preparation.pipeline
    pipeline_ref = pipeline_run.get("run", {}).get("data_preparation", {}).get("pipeline")
    if pipeline_ref:
        verify_pipeline_reference_exists(pipeline_ref)

    # 6 - https://metadata.datadrivendiscovery.org/devel/?pipeline_run#Pipeline_run_description.run.scoring.allOf.0.pipeline
    pipeline_ref = pipeline_run.get("run", {}).get("scoring", {}).get("pipeline")
    if pipeline_ref:
        verify_pipeline_reference_exists(pipeline_ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")

    time_start = datetime.datetime.now()
    action = f"Verifying pipeline run refs for pipeline run doc: {pipeline_run.get('id')}"
    logger.debug(action)
    # Nested json paths separated by . in the pipeline run schema
    pipeline_run_references_paths = ["previous_pipeline_run",
                                     "environment.reference_benchmarks",
                                     "run.data_preparation.environment.reference_benchmarks",
                                     "run.data_preparation.steps.environment.reference_benchmarks",
                                     "run.data_preparation.steps.method_calls.environment.reference_benchmarks",
                                     "run.scoring.environment.reference_benchmarks",
                                     "run.scoring.steps.environment.reference_benchmarks",
                                     "run.scoring.steps.method_calls.environment.reference_benchmarks",
                                     "steps.environment.reference_benchmarks",
                                     "steps.method_calls.environment.reference_benchmarks"]

    pipeline_run_refs = []
    for path in pipeline_run_references_paths:
        get_object_reference(pipeline_run, path.split("."), pipeline_run_refs)

    for ref in pipeline_run_refs:
        verify_pipeline_run_reference_exists(ref)
    logger.debug(f"Done {action}. Took: {query.get_time_delta(time_start)}")
    return


def get_object_reference(root_object, path_elements: list, pipeline_references: list):
    """
    Recurse through the pipeline_run dictionary object to get the reference at the specified path.
    If there are multiple references that can reside at the same path append them in a list.
    :param root_object:
    :param path_elements:
    :param pipeline_references:
    :return:
    """
    if root_object:
        if len(path_elements) == 0:
            return root_object
        if isinstance(root_object, list):
            temp_path_elements = path_elements.copy()
            for obj in root_object:
                reference = get_object_reference(obj, path_elements, pipeline_references)
                if reference:
                    pipeline_references.append(reference)
                path_elements = temp_path_elements.copy()
        else:
            elem = path_elements.pop(0)
            reference = get_object_reference(root_object.get(elem), path_elements, pipeline_references)
            if reference:
                pipeline_references.append(reference)


def verify_pipeline_run_reference_exists(pipeline_run_ref: dict):
    pipeline_run_id = pipeline_run_ref.get("id")
    if not pipeline_run_id:
        raise RuntimeError("Pipeline run ID not found in the reference")
    verify_document_exists(document_index=PIPELINE_RUNS_ALIAS, _id=pipeline_run_id)


def verify_primitive_reference_exists(primitive_ref: dict):
    primitive_id = primitive_ref.get("id")
    if not primitive_id:
        raise RuntimeError("Primitive ID not found in the reference")
    verify_document_exists(document_index=PRIMITIVES_ALIAS, id=primitive_id, digest=primitive_ref["digest"])


def verify_pipeline_reference_exists(pipeline_ref: dict):
    pipeline_id = pipeline_ref.get("id")
    if not pipeline_id:
        raise RuntimeError("Pipeline ID not found in the reference")
    verify_document_exists(document_index=PIPELINES_ALIAS, id=pipeline_id, digest=pipeline_ref["digest"],
                           _id=pipeline_ref["digest"])


def verify_dataset_reference_exists(dataset_ref: dict):
    dataset_id = dataset_ref.get("id")
    if not dataset_id:
        raise RuntimeError("Dataset ID not found in the reference")
    doc_id = hashlib.sha256("{}_{}".format(dataset_ref["digest"], dataset_ref["id"]).encode('utf8')).hexdigest()
    verify_document_exists(document_index=DATASET_ALIAS, id=dataset_id, digest=dataset_ref["digest"],
                           _id=doc_id)


def verify_problem_reference_exists(problem_ref: dict):
    problem_id = problem_ref.get("id")
    if not problem_id:
        raise RuntimeError("Problem ID not found in the reference")
    verify_document_exists(document_index=PROBLEMS_ALIAS, id=problem_id, digest=problem_ref["digest"],
                           _id=problem_ref["digest"])


def verify_document_exists(document_index, **fields):
    try:
        query.assure_document_exists(document_index, **fields)
    except query.DatabaseInconsistencyError as e:
        # Ignore DatabaseInconsistencyError during reference check as we dont want to fail if a document exists
        pass


def validate_secure_token(token):
    try:
        uuid.UUID(token)
        return True
    except ValueError:
        return False


def validate_valid_token_request(xtoken, param):
    rows = find_token(xtoken, param)
    return rows


def find_token(xtoken, username):
    token_config = TokenConfig()
    if token_config.get_token(username) == xtoken:
        return True
    return False


def process_request(request):
    submitter = request.args.get("submitter")
    document = request.get_json()
    document = query.drop_underscored_fields(document)
    response = {}
    if submitter:
        xtoken = request.headers.get('x-token')
        if xtoken is None:
            response["message"] = "Missing x-token Header"
            return response, ERROR_RESPONSE
        else:
            if not validator.validate_secure_token(xtoken):
                response["message"] = "Invalid Token"
                return response, ERROR_RESPONSE
            # Check token and _submitter match the recorded version
            if not validator.validate_valid_token_request(xtoken, submitter):
                response["message"] = "Token or Submitter do not match record"
                return response, ERROR_RESPONSE
            # Add a submitter to the record
            document = query.add_submitter(document, submitter)
    return document, OK_RESPONSE
