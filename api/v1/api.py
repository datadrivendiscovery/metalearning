from flask_restx import Api
from .endpoints.pipeline import api as pipeline_namespace
from .endpoints.pipeline_run import api as pipeline_run_namespace
from .endpoints.problem import api as problem_namespace
from .endpoints.dataset import api as dataset_namespace
from .endpoints.primitive import api as primitive_namespace
from flask import url_for
import api.v1.utils.metadata as metadata
import git
import time

# To fix hosting behind an HTTPs proxy
# https://github.com/noirbizarre/flask-restplus/issues/132
@property
def specs_url(self):
    """Fixes issue where swagger-ui makes a call to swagger.json over HTTP.
       This can ONLY be used on servers that actually use HTTPS.  On servers that use HTTP,
       this code should not be used at all.
    """
    return url_for(self.endpoint('specs'), _external=False)


def get_description():
    repo = git.Repo("../")
    commit = repo.head.commit
    remote_url = str(repo.remotes.origin.url).replace(".git", "")
    commit_datetime = time.strftime("%a, %d %b %Y %H:%M", time.gmtime(commit.committed_date))
    description = "D3M Metalearning API\nCommit: {date}\n{url}/commit/{hexsha}".format(date=commit_datetime,
                                                                                       hexsha=commit.hexsha,
                                                                                       url=remote_url)
    return description


Api.specs_url = specs_url

version = "1.0"

api = Api(
    title='Metalearning API',
    version=version,
    description=get_description(),
    doc='/{}/doc/'.format(version)
)

#
# @api.route("/")
# def homepage():
#     """
#     Redirect to version 1.0 docs
#     :return:
#     """
#     return "Hello World"


# Register namespace
api.add_namespace(dataset_namespace, path="/{}/{}".format(version, metadata.DATASET_ENDPOINT))
api.add_namespace(problem_namespace, path="/{}/{}".format(version, metadata.PROBLEM_ENDPOINT))
api.add_namespace(primitive_namespace, path="/{}/{}".format(version, metadata.PRIMITIVE_ENDPOINT))
api.add_namespace(pipeline_namespace, path="/{}/{}".format(version, metadata.PIPELINE_ENDPOINT))
api.add_namespace(pipeline_run_namespace, path="/{}/{}".format(version, metadata.PIPELINE_RUN_ENDPOINT))


