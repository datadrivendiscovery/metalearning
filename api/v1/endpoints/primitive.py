from flask import request
from flask_restx import Namespace, Resource
from api.v1.utils import query, validator, metadata as api_metadata
from api.v1.utils.query import DuplicateDocumentError
import logging
import datetime

api = Namespace("primitive", description="Primitive annotations")
logger = logging.getLogger(__name__)


@api.route("/")
class Primitive(Resource):

    parser = api.parser()
    parser.add_argument('submitter', type=str, required=False,
                        help="Optionally provide a submitter name to set the _submitter field in the doc."
                             " If this field is set, then a matching token needs to be provided "
                             "in the header `x-token`. To request a token contact d3m-jpl at list.jpl.nasa.gov")

    parser.add_argument('x-token', type=str, location='headers', help="To be used when setting the `submitter` query "
                                                                      "parameter.")

    @api.doc(responses={api_metadata.NEW_DOCUMENT_RESPONSE: "Ingested primitive",
                        api_metadata.ERROR_RESPONSE: "Ingestion error",
                        api_metadata.DUPLICATE_DOCUMENT_RESPONSE: "Duplicate document"},
             parser=parser)
    def post(self):
        """
        Post a primitive to the ES database.
        The primitive doc will be validated against the primitive schema document hosted at https://metadata.datadrivendiscovery.org/devel/?primitive

        """
        response = {}
        doc_id = None
        doc_digest = None
        try:
            request_time_start = datetime.datetime.now()
            logger.debug("Processing request for problem doc")
            process_request_response, code = validator.process_request(request)
            request_parsing_duration = query.get_time_delta(request_time_start)
            if code != api_metadata.OK_RESPONSE:
                return process_request_response, code
            else:
                doc = process_request_response
            doc_id = doc.get("id")
            doc_digest = doc.get("digest")
            time_start = datetime.datetime.now()
            validator.validate_primitive_doc(doc)
            validate_duration = query.get_time_delta(time_start)
            doc = query.add_timestamp(doc)
            time_start = datetime.datetime.now()
            resp = query.ingest_primitive_doc(doc)
            ingest_duration = query.get_time_delta(time_start)
            if not resp:
                raise RuntimeError("Error ingesting document")
        except DuplicateDocumentError:
            message = "Document with ID: {id} and digest: {digest} already exists".format(id=doc_id,
                                                                                          digest=doc_digest)
            logger.debug(message)
            response["message"] = message
            return response, api_metadata.DUPLICATE_DOCUMENT_RESPONSE
        except Exception as exp:
            message = "Error ingesting document: {}".format(exp)
            logger.exception(message)
            response["message"] = message
            return response, api_metadata.ERROR_RESPONSE
        message = "Successfully ingested document ID: {}".format(doc_id)
        response["message"] = message
        logger.debug("%(message)s. Durations"
                     "\nRequest parsing: %(request_parsing_duration)s"
                     "\tDoc Validation: %(validate_duration)s"
                     "\tDoc Ingest: %(ingest_duration)s"
                     "\tTotal Duration: %(total_duration)s", {
                         'message': message,
                         'request_parsing_duration': request_parsing_duration,
                         'validate_duration': validate_duration,
                         'ingest_duration': ingest_duration,
                         'total_duration': query.get_time_delta(request_time_start),
                     })
        return response, api_metadata.NEW_DOCUMENT_RESPONSE
