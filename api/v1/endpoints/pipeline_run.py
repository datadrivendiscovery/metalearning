from flask import request
from flask_restx import Namespace, Resource
from api.v1.utils.query import DuplicateDocumentError
from api.v1.utils import query, validator, metadata as api_metadata
import logging
import datetime

api = Namespace("pipeline-run", description="Pipeline run related operations")
logger = logging.getLogger(__name__)


@api.route("/")
class PipelineRun(Resource):
    parser = api.parser()
    parser.add_argument('submitter', type=str, required=False,
                        help="Optionally provide a submitter name to set the _submitter field in the doc."
                             " If this field is set, then a matching token needs to be provided "
                             "in the header `x-token`. To request a token contact d3m-jpl at list.jpl.nasa.gov")

    parser.add_argument('x-token', type=str, location='headers', help="To be used when setting the `submitter` query "
                                                                      "parameter.")

    @api.doc(responses={api_metadata.NEW_DOCUMENT_RESPONSE: "Ingested doc",
                        api_metadata.ERROR_RESPONSE: "Ingestion error",
                        api_metadata.DUPLICATE_DOCUMENT_RESPONSE: "Duplicate document"},
             parser=parser)
    def post(self):
        """
        Post a pipeline run to the ES database.
        The pipeline run doc will be validated against the pipeline run schema document hosted at
        https://metadata.datadrivendiscovery.org/devel/?pipeline_run

        """
        response = {}
        doc_id = None
        try:
            request_time_start = datetime.datetime.now()
            logger.debug("Processing request for problem doc")
            process_request_response, code = validator.process_request(request)
            request_parsing_duration = query.get_time_delta(request_time_start)
            if code != api_metadata.OK_RESPONSE:
                return process_request_response, code
            else:
                doc = process_request_response
            response = {}
            doc_id = doc.get("id")
            time_start = datetime.datetime.now()
            validator.validate_pipeline_run_doc(doc)
            doc = query.add_timestamp(doc)
            validate_duration = query.get_time_delta(time_start)
            time_start = datetime.datetime.now()
            resp = query.ingest_pipeline_run_doc(doc)
            ingest_duration = query.get_time_delta(time_start)
            if not resp:
                raise RuntimeError("Error ingesting document")
        except DuplicateDocumentError:
            message = "Document with ID: {id} already exists".format(id=doc_id)
            logger.debug(message)
            response["message"] = message
            return response, api_metadata.DUPLICATE_DOCUMENT_RESPONSE
        except Exception as exp:
            message = "Error ingesting document: {}".format(exp)
            logger.exception(message)
            response["message"] = message
            return response, api_metadata.ERROR_RESPONSE
        message = "Successfully ingested document ID: {}".format(doc_id)
        response["message"] = message
        logger.debug("%(message)s. Durations"
                     "\nRequest parsing: %(request_parsing_duration)s"
                     "\tDoc Validation: %(validate_duration)s"
                     "\tDoc Ingest: %(ingest_duration)s"
                     "\tTotal Duration: %(total_duration)s", {
                         'message': message,
                         'request_parsing_duration': request_parsing_duration,
                         'validate_duration': validate_duration,
                         'ingest_duration': ingest_duration,
                         'total_duration': query.get_time_delta(request_time_start),
                     })
        return response, api_metadata.NEW_DOCUMENT_RESPONSE
