import os
from configparser import ConfigParser
import logging
import csv

logger = logging.getLogger(__name__)


class Singleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class APIConfiguration(metaclass=Singleton):
    def __init__(self):
        config_file = 'app.conf.ini'
        current_path = os.path.dirname(os.path.realpath(__file__))
        config_file = os.path.join(current_path, config_file)
        if not os.path.isfile(config_file):
            logger.error(f'Config file {config_file} not found')
            raise RuntimeError(f'Config file {config_file} not found')
        temp_configs = ConfigParser()
        temp_configs.read(config_file)
        self._config = temp_configs
        pass

    def get_item(self, key, profile='default', default=None):
        return self._config.get(profile, key, fallback=default)
    pass


class TokenConfig(metaclass=Singleton):
    def __init__(self):
        self._token = dict()
        config = APIConfiguration()
        with open(config.get_item("token_filepath")) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            for row in csv_reader:
                self._token.update({row['submitter']: row['token']})

    def get_token(self, submitter):
        return self._token.get(submitter, None)


class ElasticsearchConfig(metaclass=Singleton):
    def __init__(self):
        config = APIConfiguration()
        self._host = config.get_item("ELASTICSEARCH_HOST")
        self._port = int(config.get_item("ELASTICSEARCH_PORT"))
        self._timeout = int(config.get_item("ELASTICSEARCH_TIMEOUT"))

    def get_host(self):
        return self._host

    def get_port(self):
        return self._port

    def get_timeout(self):
        return self._timeout
