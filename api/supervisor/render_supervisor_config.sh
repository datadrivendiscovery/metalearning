#!/bin/bash
#Builds the supervisor config file

basedir=$( cd "$(dirname "$0")" ; pwd -P )
API_PATH=$(dirname ${basedir})
LOGS_DIR=${API_PATH}/logs
mkdir -p ${LOGS_DIR}

super_conf_path=${basedir}/supervisord.conf
super_template_path=${basedir}/supervisor.conf.tmpl

echo "building supervisor.conf file"
#load the template
template="$(cat ${super_template_path})"
#fill the template
eval "echo \"${template}\"" > ${super_conf_path}

chmod 755 ${super_conf_path}
echo "Built supervisor.conf file"
