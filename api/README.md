# Installation 

```bash
git clone https://gitlab.com/datadrivendiscovery/metalearning.git
cd metalearning/api
pip install -r requirements.txt
```

# Configuration

The api runs with a default configuration which binds it to `0.0.0.0` on port `5000`.
To change this edit the [gunicorn.conf.py](gunicorn.conf.py) file. 

To change the logging configuration edit the [logging.conf](logging.conf) file. 

# Running the api

```bash
cd supervisor
./render_supervisor_config.sh
supervisord -c supervisord.conf
```
