import argparse
import csv
import uuid
import os
from configparser import ConfigParser

config_file = "app.conf.ini"
current_path = os.path.dirname(os.path.realpath(__file__))
config_file = os.path.join(current_path, config_file)
if not os.path.isfile(config_file):
    print("Config file {} not found".format(config_file))
    exit(1)


def main(args):
    configs = ConfigParser()
    configs.read(config_file)
    config = dict(configs.items(args.profile))
    token = str(uuid.uuid4())
    username = args.username
    token_file_path = config.get("token_filepath")
    fieldnames = ['token', 'submitter']
    write_header = False
    if not os.path.exists(token_file_path):
        write_header = True
    with open(token_file_path, mode='a') as employee_file:

        employee_writer = csv.DictWriter(employee_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL,
                                         fieldnames=fieldnames)
        if write_header:
            employee_writer.writeheader()
        employee_writer.writerow({"token": token, "submitter": username})
    employee_file.close()
    print("Generated token for user {}\n{}".format(username, token))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--username", help="Username to associate token with", required=True)
    parser.add_argument("--profile", help="Config profile to use", default="default")
    args = parser.parse_args()
    main(args)
