from flask import Flask, redirect, url_for, send_from_directory, render_template
from flask_cors import CORS, cross_origin
import logging
from configparser import ConfigParser
import sys
import os
from api.v1.api import api
from werkzeug.middleware.proxy_fix import ProxyFix
import uuid
from flask import request, flash
import csv
from api.api_conf import APIConfiguration


current_path = os.path.dirname(os.path.realpath(__file__))

# Initialize logger
logging.basicConfig(level=logging.DEBUG)
logger = logging.getLogger(__name__)

app = Flask(__name__, template_folder='static')
app.wsgi_app = ProxyFix(app.wsgi_app)
CORS(app)


@app.route("/")
def homepage():
    """
    Redirect to version 1.0 docs
    :return:
    """
    base_path = os.getcwd()
    print(os.path.join(base_path, "static"))
    return send_from_directory(os.path.join(base_path, "static"), "index.html")


api.init_app(app)


def init(profile="default"):
    global PROFILE
    PROFILE = profile
    global CONF
    CONF = APIConfiguration()


def getConf(key):
    global PROFILE
    return CONF.get_item(key, PROFILE)


if __name__ == '__main__':

    profile = "default"
    if len(sys.argv) > 1:
        profile = sys.argv[1]
    init(profile)
    import logging

    logging.basicConfig(filename='metalearning_api.log', level=logging.DEBUG)
    app.run(host=getConf("FLASK_HOST"), port=int(getConf("FLASK_PORT")), debug=getConf("DEBUG") == 'True', threaded=True)
