# Metalearning

To facilitate better metalearning we can collaboratively build a dataset
useful for metalearning. Such dataset would consist of samples of the following:
 * problem
 * dataset
 * pipeline
 * runtime arguments used
 * outcome
 * other metadata

The aim of this repository is to standardize how this data is represented
so that it can be useful to all performers. Moreover, for easier sharing and
consumption of this data, some common utilities and services might be developed.

## Considerations

* Pipelines should describe also interactions by the user.
  * We should store some user identifier so one can learn across users.
* All pipelines we are collecting should be ran pipelines at least once.
  * Ideally, outcome object would be (approximately )the same if pipeline is rerun with same resources available.
* Outcome object should contain:
  * metrics on input data
  * resources used for running
    * time, memory, compute used
    * per primitive and for the whole pipeline
  * user identifier
  * did user choose the pipeline
  * a textual description why user chose the pipeline
  * failure - why it failed
    * for example, out of memory issue
  * when was it discovered:
    * during pretraining
    * during evaluation
* Same pipeline descriptions can be used also in TA2-TA3 API:
  * To describe the pipeline to the user.
  * To tell TA2 system which pipeline to build using placeholders (and TA2 system then autocompletes the pipeline).
* We should also include attribution for pipelines:
  * Which team did it, source.
  * Timestamp.
* How we standardize same cross validation and general loop of preprocessing (dataset splitting) and score computation,
  so that outcome object is useful and share same properties? If every pipeline can be anything, then outcomes will
  be hard to compare for the same dataset. But if we know that dataset splitting and score computation is done the same,
  then we can compare outcome objects.

# Database structure

Because there are one-to-many and many-to-many relations between pipeline run and problem, dataset, and pipeline,
those three are stored each in its own database collection and referenced from a pipeline run document.
So in JSON, one pipeline run could be represented as:

```yaml
{
  "id": <pipeline run ID, uuid based on content without id field>,
  "schema": <overall pipeline run schema version/URI>,
  "problem": {
    "id": <problem document ID, chosen by creator>,
    "digest": <sha256 hash of problem document>
  },
  "datasets": [
    {
      "id": <dataset document ID, chosen by creator>,
      "digest": <sha256 hash of dataset description and files>
    }
  ],
  "pipeline": {
    "id": <pipeline document ID, chosen by creator>,
    "digest": <sha256 hash of pipeline document>
  },
  <pipeline run fields>
}
```

Problem, dataset, and pipeline documents are referenced by both `id` and
`digest` at the top-level of the pipeline run document. They are referenced
just by `id` at deeper levels.

For future compatibility, each run can reference multiple datasets.

Pipeline is a reference as well because for each pipeline multiple runs on different set of resources and hyper-parameter
configurations can be made.

In storage, we can denormalize database structure and inline referenced objects to make queries
easier, if we will use a database which does not support joins.

The id for pipeline run document is generated in a consistent, reproducible way,
using UUID. The `id` is generated on pipeline run documents without the ID field and then
validated with the `id` field. The `id` can also be used to de-duplicate
documents.


## Problem

We use [problem description](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/problem.json)
to describe problems. Because problem IDs are not necessary unique (there can
be multiple versions with same problem ID), we use `digest` to identify a
particular problem. Example:

```yaml
{
  "id": "iris_problem_1",
  "digest": <sha256 hash of document>,
  "version": "1.0",
  "description": "Distinguish Iris flowers of three related species.",
  "name": "Distinguish Iris flowers",
  "problem": {
    "performance_metrics": [
      {
        "metric": "ACCURACY"
      }
    ],
    "task_subtype": "MULTICLASS",
    "task_type": "CLASSIFICATION"
  },
  "inputs": [
    {
      "dataset_id": "iris_dataset_1",
      "targets": [
        {
          "target_index": 0,
          "resource_id": "0",
          "column_index": 5,
          "column_name": "species"
        }
      ]
    }
  ],
  "outputs": {
    "predictions_file": "predictions.csv",
    "scores_file": "scores.csv"
  }
}
```

It might happen that there exist multiple equal or equivalent problem documents with different problem document IDs in the
metalearning database. This is on the user of the database to detect and resolve (using their definition of equivalence).

## Dataset

We use [dataset schema](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/container.json)
to describe datasets. Similar to problems, also dataset IDs are not unique, so
the pairing of `id` and `digest` identifies a particular dataset. Dataset
document stored in the database could consist of all top-level metadata (and
metafeatures) of a dataset, including metadata necessary to retrieve a copy of
a dataset. But when using common D3M datasets only two fields necessary to
identify a dataset are required. Example for full top-level metadata:

```yaml
{
  "id": "iris_dataset_1",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/container.json",
  "version": "1.0",
  "digest": "b6e903f0482c9e342112dda0961c2bc9544dfad4535d3099ab29741b34c62d55",
  "structural_type": "d3m.container.dataset.Dataset",
  "name": "Iris Dataset",
  "location_uris": [
    "https://gitlab.com/datadrivendiscovery/tests-data/raw/master/datasets/iris_dataset_1/datasetDoc.json"
  ],
  "dimension": {
    "name": "resources",
    "semantic_types": [
      "https://metadata.datadrivendiscovery.org/types/DatasetResource"
    ],
    "length": 1
  },
  "source": {
    "license": "CC",
    "redacted": false,
    "human_subjects_research": false
  }
}
```

And only required fields:

```yaml
{
  "id": "iris_dataset_1",
  "schema": "https://metadata.datadrivendiscovery.org/schemas/v0/container.json",
  "digest": "b6e903f0482c9e342112dda0961c2bc9544dfad4535d3099ab29741b34c62d55",
  "structural_type": "d3m.container.dataset.Dataset"
}
```

It might happen that there exist multiple equal or equivalent dataset documents with different dataset document IDs in the
metalearning database. This is on the user of the database to detect and resolve (using their definition of equivalence).
In the case of datasets, one can use dataset's digest to determine which datasets share the same dataset description
and files.

Dataset digest is computed over the stored dataset in the D3M dataset format and includes both metadata and data (files).

## Pipeline

We use [pipeline schema](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/pipeline.json)
to describe a pipeline. Pipeline `id`s are not unique, so `digest` is also used
to uniquely identify a pipeline.

Moreover, equal or equivalent pipelines can happen to exist in the database even with different pipeline IDs.
It is not required from systems to assure that there are no duplicates of this kind either. Generally
the idea is simple: if a pipeline was generated by your system, create a new entry into the database for it,
listing yourself as an author, and store results of running it. If you find a pipeline by searching over this
database, then do not create a new entry, but just record results you got running it.

For a pipeline run, all primitives and subpipelines have to be exactly resolved (including the digest).
When the same system both produced a pipeline and run it, this is easy to assure. But if pipelines
are run later on, exact same primitives and subpipelines might not be found. In that case the system
should resubmit a new pipeline with updated primitives and subpipelines and then submit a pipeline
run for that updated pipeline.

## Pipeline run fields

### Steps and their execution

During run of a pipeline, primitives receive a `hyperparams` constructor argument, and
their methods can receive also extra arguments which do not depend on pipeline data. They
might be runtime arguments or overriding hyper-parameters for a given call (arguments of
kind `RUNTIME` and `HYPERPARAMETER` in primitive's metadata, respectively). `steps` field
provides values for all of those. Moreover, each method can be called multiple times, in
an iterative fashion. This is also recorded in this field. Steps also record information
about their execution. In JSON:

```yaml
# In same order as they are listed in the pipeline. Each step
# corresponds to a step in the pipeline, order-wise.
"steps": [
  {
    # Does this step corresponds to a primitive or a subpipeline?
    "type": <"PRIMITIVE", "SUBPIPELINE">,
    # Together with hyper-parameters listed as part of a pipeline they complete all values necessary to
    # instantiate "hyperparams" constructor argument. All hyper-parameter values have to be listed explicitly,
    # even if the value matches the default value of a hyper-parameter.
    "hyperparams": {
      <map between hyper-parameter names and their values>
    },
    "random_seed": <integer>,
    # Information about method calls, in order in which they were called.
    "method_calls": [
      {
        "name": "__init__",
        # Arguments to constructor should not be provided, because they are provided by runtime
        # and are runtime specific (paths to volumes, etc.).
        ...
      },
      {
        "name": "set_training_data"
        # Pipeline arguments to methods are provided in a standard way. But methods can
        # have additional runtime arguments or arguments overriding hyper-parameters for a call.
        # Those values have to be explicitly listed here.
        "arguments": {}
        # Python LogRecord entries during a method run.
        # Other custom fields are allowed (Python LogRecord can be extended with custom fields).
        "logging": [
          # See: https://docs.python.org/3/library/logging.html#logging.LogRecord
          {
            "name": ...,
            "msg": ..., # Non-interpolated message.
            "args": [ ], # Arguments for message interpolation, when JSON-serializable.
            "levelname": ...,
            "levelno": ...,
            "pathname": ...,
            "filename": ...,
            "module": ...,
            "exc_text": ..., # Python exception and formatted stack trace as text.
            "exc_type": ..., # Python exception name.
            "stack_info": ..., # Formatted stack trace as text.
            "lineno": ...,
            "funcName": ...,
            "created": ...,
            "msecs": ...,
            "relativeCreated": ...,
            "thread": ...,
            "threadName": ...,
            "processName": ...,
            "process": ...,
            "message": ..., # Interpolated message.
            "asctime": ...
          },
          ...
        ],
        # Metadata of the "CallResult" value (or "MultiCallResult" values) if the method
        # call returns a container type.
        "metadata": {
          # For "CallResult", we store metadata under "value" key.
          "value": [
            {
              # Serialized metadata as selector/metadata pairs.
              "selector": <selector>,
              "metadata": <metadata>
            }
          ],
          # For "MultiCallResult", keys should match "values" names.
          ...
        }
        # Status for the method call.
        "status": {
          "state": <"SUCCESS" or "FAILURE">,
          "message": <failure message>
        },
        "start": <method run start, absolute timestamp>,
        "end": <method run end, absolute timestamp>
      },
      {
        "name": "fit",
        "arguments": {
          # Let's assume this primitive has a hyper-parameter called "threshold" and an argument on
          # "fit" allowing one to override it. Then here we record which value was used in this call
          # for this argument.
          "threshold": <value>,
          # The following are standard runtime arguments, we list their defaults values we used.
          "timeout": null,
          "iterations": null
        }
      },
      {
        "name": "fit",
        # Specifying "calls" allows one to combine multiple calls with same arguments into one record.
        # In this example, this means that this method has been called twice more.
        "calls": 2,
        "arguments": {
          "threshold": <value>,
          "timeout": null,
          "iterations": null
        }
        ...
      },
      {
        "name": "produce",
        "arguments": {
          "timeout": null,
          "iterations": null
        },
        # optional environment if this method_call was run in a different
        # than the global or step level environment
        "environment": <same as top-level environment structure>
        ...
      }
    ],
    # If this step is a sub-pipeline, then here come steps of a sub-pipeline, recursively.
    # In this case field above are not populated.
    "steps": [
      ...
    ],
    # The following fields are used both for "PRIMITIVE" and "SUBPIPELINE".
    "status": {
      "state": <"SUCCESS" or "FAILURE">,
      "message": <failure message>
    },
    "start": <step run start, absolute timestamp>,
    "end": <step run end, absolute timestamp>
    # optional environment if this step was run in a different environment than
    # the global environment specified at the top level
    "environment": <same as top-level environment structure>
  }
],
# Status for the whole pipeline run.
"status": {
  "state": <"SUCCESS" or "FAILURE">,
  "message": <failure message>
}
"start": <pipeline run start, absolute timestamp>,
"end": <pipeline run end, absolute timestamp>
```

### Run fields

Run fields describe how a pipeline was run and results.
If pipeline is run multiple times during evaluation, each run should have its own pipeline run entry.
In JSON, the following is a sketch of its representation:

```yaml
"run": {
  # Custom phases are allowed (like one for end-to-end refinement).
  # In the case of custom phases mostly only all method calls should be recorded and results only if it produces any.
  "phase": <a string like "FIT", "PRODUCE", or some other string representing a phase this pipeline run is associated with>,
  "fold_group": {
    "id": <UUID of the fold group of pipeline runs belonging together, e.g., part of the same cross-validation evaluation run>,
    # A fold number for this pipeline run. If not part of the cross-validation, this can be set to 0.
    "fold": <integer>
  },
  # If a data preparation pipeline was used in this pipeline run, we reference the pipeline here.
  "data_preparation": {
    "pipeline": {
      "id": <ID of the pipeline which prepares data>,
      "digest": <sha256 hash of pipeline document>
    },
    "steps": [
      # Same steps record as for the regular pipeline (random_seed, hyperparams, status, etc.).
      ...
    ],
    # status for the data preparation pipeline.
    "status": {
      "state": <"SUCCESS" or "FAILURE">,
      "message": <failure message>
    },
    "start": <data preparation pipeline run start, absolute timestamp>,
    "end": <data preparation pipeline run end, absolute timestamp>
  },
  # This pipeline can be used for any pipeline run type, because it computes scores.
  "scoring": {
    "pipeline": {
      "id": <ID of the pipeline which processes outputs from the pipeline to compute the score>,
      "digest": <sha256 hash of pipeline document>
    },
    "steps": [
      # Same steps record as for the regular pipeline (random_seed, hyperparams, status, etc.).
      ...
    ],
    # status for the scoring pipeline.
    "status": {
      "state": <"SUCCESS" or "FAILURE">,
      "message": <failure message>
    },
    "start": <scoring pipeline run start, absolute timestamp>,
    "end": <scoring pipeline run end, absolute timestamp>
  },
  # If a phase produces any results and/or scores, we record them.
  "results": {
    # Scores should the match the output of the scoring pipeline.
    # Any custom metric name should match the metric name in the scoring pipeline output.
    "scores": [
      {
        "metric": {
          "metric":  <metric name>,
          "params": {
            "k": ...,
            "pos_label": ...,
          }
        },
        "value": <value>,
        "normalized": <normalized value between 0 and 1, higher is better>
      }
    ],
    # Produced data for this run.
    "predictions": {
      "header": [
        <a list of column names>
      ],
      "values": [
        <a list of predictions themselves, every element is a list of column values, in Lincoln Labs predictions format>
      ]
    }
  }
}
```

Preparation of the data and any postprocessing should be described using standard pipelines for
known evaluation approaches. Preparation pipeline maps input datasets to output datasets in a
way that the whole pipeline is optional if data was not changed before being passed to the main
pipeline. Similarly, scoring pipeline is used only to compute scores from predictions.

### Context fields

There are additional fields to store other metadata for a pipeline run. In JSON:

```yaml
"context": <"PRETRAINING", "TESTING", "EVALUATION", "PRODUCTION">,
# References a pipeline run that occurred immediately before this pipeline run.
# Used for reproducibility, for example a test run would reference the train
# run. If it is not provided, it indicates the first pipeline run.
"previous_pipeline_run": {
  "id": <pipeline_run document ID>
}
"users": [
  {
    "id": <Globally unique ID for this user. It can be opaque, but it should identify the same user across sessions. Consider using UUID variant 5 with namespace set to the name of your system and name to an ID in your system's database.>,
    "reason": <A natural language description of what the user did to be on the list, e.g., \"Picked a pipeline from a list of pipelines.\".>
    "rationale": <A natural language description by the user of what the user did, e.g., \"I picked a pipeline because it looks short in comparison with others.\".>
  }
],
# A description of the environment in which this pipeline run occurred. It is
# assumed that all `steps` and `method_calls` were run in this environment,
# unless an environment is specified at the step or method_call level.
"environment": {
  "id": <UUID based on `environment`>

  # ID is decided by whoever runs it, but it should be globally unique.
  # The idea is that the worker specifies the system inside which the pipeline
  # is run so that multiple runs on the same system can be grouped together.
  "worker_id": <globally unique ID for the worker>,

  # A git version of your (or reference, if directly using it) engine. This can
  # be useful for the author of the pipeline run to record, but is less useful
  # for others. For others, "reference_engine_version" is probably more useful.
  "engine_version": <git commit hash of the engine version used>,

  # If your engine is subclassing a reference engine, list its git version here.
  "reference_engine_version": <git commit hash of the reference engine version based on>,

  # If a pipeline is run inside a Docker container, this field should specify
  # the Docker image used to run this pipeline. This can be useful for
  # the author of the pipeline run to record, but is less useful for others.
  # For others, "base_docker_image" is probably more useful.
  "docker_image": {
    # Docker image name including a label, and optionally prefixed with a registry.
    "image_name": <value>,
    "image_digest": <SHA256 Docker image digest>
  },

  # If a pipeline is run inside a Docker container which is based on a public
  # image or known base image, then this field should specify that Docker image.
  # I.e., if your system is using a private Docker image but is extending a
  # "complete" Docker image, then list the "complete" Docker image here.
  "base_docker_image": {
    # Docker image name including a label, and optionally prefixed with a registry.
    "image_name": <value>,
    "image_digest": <SHA256 Docker image digest>
  },

  # Resources available for this environment.
  "resources": {
    "cpu": {
      "devices": [
        {
          "name": <phycical CPU device name>
        }
      ],
      "physical_present": <how many physical CPU cores are available>,
      "logical_present": <how many logical CPU cores are available>,
      # See https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-cpu
      # for more information.
      "configured_available": <the amount of CPU resources available to the pipeline runtime in Kubernetes CPU units or equivalent>,
      "constraints": {
        <any constraints as found in the cgroups (e.g. inside of a resource limited docker container)>
      }
    },

    "memory": {
      "devices": [
        {
          "name": <phycical memory device name>,
          "memory": <memory in bytes for this device>
        }
      ],
      "total_memory": <how much memory is available in bytes>,
      # See https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-memory
      # for more information.
      "configured_memory": <the amount of memory available to the pipeline runtime in Kubernetes memory units or equivalent>,
      "constraints": {
        <any constraints as found in the cgroups (e.g. inside of a resource limited docker container)>
      }
    },

    "gpu": {
      "devices": [
        {
          "name": <physical GPU device name>,
          "memory": <memory in bytes for this GPU>
        }
      ],
      "total_memory": <how much GPU memory is available in bytes>,
      # See https://kubernetes.io/docs/concepts/configuration/manage-compute-resources-container/#meaning-of-memory
      # for more information.
      "configured_memory": <the amount of GPU memory available to the pipeline runtime in Kubernetes memory units or equivalent>,
      "constraints": {
        <any constraints as found in the cgroups (e.g. inside of a resource limited docker container)>
      }
    },

    # Reference benchmarks are pipeline runs of standard and optional additional benchmark
    # pipelines which should be run on the worker during same or equivalent session so that
    # this pipeline run can be expected to have the same timing characteristics. If it is
    # known that worker configuration has not been changed between sessions, benchmark
    # pipeline runs can be reused.
    "reference_benchmarks": [
      {
        "id": <pipeline run IDs>
      }
    ]
  }
}
```

## Document restrictions

On top of JSON schema for each of four document types defined, there are additional restrictions
not captured by JSON schema for documents submitted to the metalearning database.

### Problem description

* `schema` has to be `https://metadata.datadrivendiscovery.org/schemas/v0/problem.json`.
* `digest` field is required.
* `digest` of the document has to match the computed digest of the document.
* Problem description is represented in "canonical structure".

### Dataset

* `schema` has to be `https://metadata.datadrivendiscovery.org/schemas/v0/container.json`.
* `id` field is required.
* `digest` field is required.
* `structural_type` has to be `d3m.container.dataset.Dataset`.

Note: dataset digest is computed over both dataset description and files as stored in D3M dataset format.

### Pipeline

* `schema` has to be `https://metadata.datadrivendiscovery.org/schemas/v0/pipeline.json`.
* All `digest` fields are required (top-level pipeline field, and fields in primitive and sub-pipeline references).
* `digest` of the document has to match the computed digest of the document.
* There are no `PLACEHOLDER` steps.
* Any `SUBPIPELINE` step should reference the pipeline and not nest it. Only `id` and `digest` fields
  should be present in the reference.
* Pipeline should pass `Pipeline.check` without resolving primitives or pipelines (a very simple check).
* Pipeline is represented in "canonical structure".
* Any referenced pipeline should already exists in the metalearning database.

### Pipeline run

* `schema` has to be `https://metadata.datadrivendiscovery.org/schemas/v0/pipeline_run.json`.
* `id` of the document has to match the computed hash ID of the document.
* `datasets` references should be just references and not embedded documents.
* `pipeline` references should be just references and not embedded documents.
* `problem` reference should be just a reference and not an embedded document.
* `pipeline_run` references should be just references and not embedded documents.
* Any failure should be propagated to parent (`status` field should be correctly set).
* Start and end timestamps should be in right order.
  (Start of primitive step can be only sooner or at the start of the first method call start timestamp.)
* `steps` and `method_calls` arrays are required when the appropriate status is `SUCCESS`.
* There is no overlap in hyper-parameters between those listed in the pipeline and those listed in pipeline run.
* `start` and `end` timestamps have to be present (they are currently optional in the schema).
* `random_seed` values have to be present (they are currently optional in the schema).
* Any referenced pipeline should already exists in the metalearning database.
* A referenced previous pipeline run should already exists in the metalearning database.
* A referenced problem description should exists in the metalearning database.
* Referenced datasets should exist in the metalearning database.

## Running Pipelines and Inserting Results into the Database

### The D3M Pipeline and Pipeline Run Schema Language

The pipeline schema is a language for describing a pipeline unambiguously. It describes which primitives are used, how they are composed, which functions are called, the hyper-parameters used, etc. This includes ID, versioning, and digest information for complete reproducibility. The pipeline schema documentation can be found [here](https://docs.datadrivendiscovery.org/devel/pipeline.html#) and the actual schema is found [here](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/pipeline.json).

The pipeline run schema documents the results of running a pipeline, independent of any implementation. It includes references to a pipeline document, a problem document, a dataset document, and fields containing information about execution steps, runtime environment, predictions, scores, and more. There are also references to a pipeline for preparing data (train/test splits, cross validation folds, etc.) and a pipeline for scoring data. The actual schema for pipeline runs is located [here](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/metadata/schemas/v0/pipeline_run.json).

As an analogy, the pipeline schema can be compared to a programming language. A pipeline document is then an instance of the language or a particular program. The pipeline run document is the output and stack trace resulting from running the pipeline program and includes information about any at-runtime provided arguments (like command line arguments, file inputs).

A pipeline run document represents one phase of running a pipeline. These phases are called `FIT` and `PRODUCE` and correspond to train and test phases. The pipeline run document contains a reference to previous pipeline runs so that the `PRODUCE` pipeline run can point to the record of how the pipeline was fitted, i.e. the `FIT` pipeline run. This enables complete reproducibility.

### Running Pipelines With The D3M Reference Runtime

Continuing the analogy above, the reference runtime is an interpreter for the pipeline schema language. It provides a standard for pipeline schema interpretation, so implementation focuses on correctness rather than speed. It outputs a pipeline run document. For convenience, it also outputs the predictions in a separate document. Note that the pipeline run document also contains these predictions. Documentation for the reference runtime can be found [here](https://docs.datadrivendiscovery.org/devel/pipeline.html#reference-runtime) and the code resides [here](https://gitlab.com/datadrivendiscovery/d3m/blob/devel/d3m/runtime.py).

The reference runtime requires configuration arguments as input, to know which pipeline, dataset, problem description, etc. to use/run. Since all of these components can be found in the pipeline run, an existing  pipeline run document can serve also as a source of configuration for another run. Any hyper-parameters not specified by the input arguments (a.k.a. "free hyper-parameters") will be set to the default values specified by the primitive metadata and recorded in the output pipeline run. The reference runtime executes the input pipeline and outputs a pipeline run document. The output pipeline run document is generated using the information passed into the runtime and the metadata collected throughout pipeline execution. The runtime runs a pipeline twice, for `FIT` and `PRODUCE` phases, obtaining both train and test results, producing two pipeline run documents, as described above. It is first run with a call to `fit` and `produce` methods for every pipeline step to train the pipeline and then run with a call only to `produce` method for every pipeline step to obtain test results. Between the phases, the fitted state of the pipeline is stored internal to the runtime in memory.

### Running and Scoring a Pipeline

The reference runtime also supports computing scores (e.g. F1 Macro, Accuracy, etc.) on predictions, when given a scoring pipeline and scoring data, i.e. the known true targets. The following is an example of using the reference runtime from the command line to run the `FIT` and `PRODUCE` phases as well as score the predictions from the PRODUCE phase. The scoring pipeline, pipeline to run, metadata of the primitives used by the pipelines, datasets, and problem have to be normalized before running the pipeline so that we have all necessary documents for submitting the pipeline run to the metalearning database, and so the documents are in the proper format.

Here are examples of normalizing a pipeline and its associated documents, then fitting, producing, and scoring the pipeline. There is an example using the D3M CLI and one using the D3M Python API. 

#### Normalizing, Fitting, and Scoring Pipelines Using the CLI

To normalize a dataset file:

```
python3 -m d3m dataset describe /path/to/dataset/datasetDoc.json > normalizedDatasetDoc.json
```

Note that all dataset files (i.e. train, test, and scoring) used in evaluating the pipeline should be normalized.

To normalize a problem file:

```
python3 -m d3m problem describe /path/to/problem/problemDoc.json > normalizedProblemDoc.json
```

To normalize a primitive, the primitive should first be registered in your D3M index. Then you can run:

```
python3 -m d3m primitive describe <primitive python path or ID> > normalizedPrimitiveDoc.json
```

To normalize the pipeline you wish to run:

```
python3 -m d3m --pipelines-path /path/to/subpipelines/output/directory pipeline describe /path/to/pipelines/pipeline.yml > pipeline.json
```

Note that any sub-pipelines used have to be normalized as well. Also note that it is not required to normalize the documents to use the d3m runtime to run a pipeline, but it is required if it is desired to submit the resulting pipeline run document to the metalearning database.

Once the documents are normalized, the d3m runtime can be used to fit, produce, and score the pipeline, generating a pipeline run document in the process. Here is an example:

```bash
python3 -m d3m \
    --pipelines-path /path/to/subpipelines/output/directory \
    # Strict resolving and digest assures that you are
    # really using normalized inputs
    --strict-resolving \
    --strict-digest \
  runtime \
    # If your pipeline is using any primitives that require external files,
    # suppy the path to that directory here
    --volumes /volumes \
    --worker-id <some global ID of the machine on which the run is done> \
  fit-score \
    --scoring-pipeline normalizedScoringPipeline.json \
    --pipeline normalizePipelineToRun.json \
    --problem normalizedProblemDoc.json \
    # The train dataset
    --input normalizedTrainDatasetDoc.json \
    # The test data set with the target column redacted
    --test-input normalizedTestDatasetDoc.json \
    # The test data set with the target column untouched
    --score-input normalizedScoreDatasetDoc.json \
    # Path to save the resulting pipeline run to
    --output-run pipeline_runs.json \
    # Path to save the pipeline run's predictions to
    --output predictions.csv \
    # Path to save the metric scores (e.g. RMSE, F1, etc.) to
    --scores scores.csv
```

#### Normalizing, Fitting, and Scoring Pipelines Using The Python Interface

In essence, to normalize a dataset, problem, pipeline, or primitive's metadata, call its `to_json_structure` method.

Datasets should be instances of `d3m.container.Dataset`. To normalize a dataset (setting `canonical` removes information about the local path to the dataset):

```python
normalized_dataset = dataset.to_json_structure(canonical=True)
```

Problems should be instances of `d3m.metadata.problem.Problem`. To normalize a problem (setting `canonical` removes information about the local path to the problem):

```python
normalized_problem = problem.to_json_structure(canonical=True)
```

Primitives can be classes or instantiations of classes inheriting from `d3m.primitive_interfaces.base.PrimitiveBase`. To normalize a primitive:

```python
normalized_primitive_metadata = primitive.metadata.to_json_structure()
```

Pipelines should be instances of `d3m.metadata.pipeline.Pipeline`. To normalize any pipeline:

```python
normalized_pipeline = pipeline.to_json_structure()
```

To fit, produce, and score a pipeline using the Python interface, use the [D3M reference runtime](https://docs.datadrivendiscovery.org/devel/pipeline.html#reference-runtime).

### Inserting Documents Into The Database

Before a pipeline run can be submitted to the database, any dependent documents it references must first exist in the database. These document types have dependencies:

- **Pipeline:** Before a pipeline can be submitted, all primitives and subpipelines it is composed of must first exist in the database.
- **Pipeline Run:** All of a pipeline run's datasets, its problem doc, and its pipelines (data preparation, scoring, and its main pipeline), must exist in the database before the run can be submitted.   
- **Dataset:** Optional source dataset reference must first exist in the database before the dataset can be submitted.
- **Problem:** Optional source problem reference must first exist in the database before the problem can be submitted.

To insert a document into the database, make a POST request to the HTTP API endpoint dedicated to that document type. Documentation for the full API can be found [here](https://metalearning.datadrivendiscovery.org/1.0/doc/). Please ensure to submit the normalized form of that document.

The database API will return a 201 HTTP status code if the document was written successfully, and a 202 code if the document already exists. In either case, the document will exist in the database as it needs to in order for other documents to reference it, or in order for a user to query it.

#### Submitting Using Python

Here is an example of submitting each document type to the database using Python:

```python
import requests

from d3m.metadata.pipeline import PrimitiveStep

API = "https://metalearning.datadrivendiscovery.org/1.0"
HEADERS = {"Accept": "application/json", "Content-Type": "application/json"}

# Save all primitives in a pipeline
for step in pipeline.steps:
  if isinstance(step, PrimitiveStep):
    requests.post(
      API + "/primitive",
      json=step.primitive.metadata.to_json_structure(),
      headers=HEADERS,
    )

# Save the pipeline
requests.post(
  API + "/pipeline",
  json=pipeline.to_json_structure(),
  headers=HEADERS,
)

# Save a dataset
requests.post(
  API + "/dataset",
  json=dataset.to_json_structure(canonical=True),
  headers=HEADERS
)

# Save a problem
requests.post(
  API + "/problem",
  json=problem.to_json_structure(canonical=True),
  headers=HEADERS
)

# Save a pipeline run
requests.post(
  API + "/pipeline-run",
  # The reference runtime returns pipeline runs
  # already as a json structure
  json=pipeline_run,
  headers=HEADERS
)
```

#### Submitting Via Command Line

Here is an example of submitting a pipeline file to the database using `curl`.

```bash
curl -X POST "https://metalearning.datadrivendiscovery.org/1.0/pipeline/" \
  -H "Accept: application/json" \
  -H "Content-Type: application/json" \
  -d @path/to/pipeline.json

# {
#     "message": "Successfully ingested document ID: ..."
# }
```

#### Submitting And Record Submitter

You can optionally record a submitter by setting the `_submitter` field in any document.
If this field is set, then a matching token needs to be provided in the header `x-token`.
To register a submitter and a corresponding token contact d3m-jpl at list.jpl.nasa.gov.

### Providing Information to Running Docker Images

One requirement of the pipeline run schema is to record the name and digest of the docker image used 
to run the pipeline.  Since the runtime needs to record this automatically when the pipeline is run, 
it needs to be able to access this docker image info from inside the docker container. To accomplish 
this, when using the reference runtime inside a docker container, the caller must pass in the name 
and digest as environment variables when the container is run.

Four environment variables are available for this purpose:

* `D3M_BASE_IMAGE_NAME`: This is the full name and tag of the base D3M image. An example of this is the image
`registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.5.18-20200612-000030`
* `D3M_BASE_IMAGE_DIGEST`: This is the digest of the D3M base image which can be obtained by running 
`docker images --digests`
* `D3M_IMAGE_NAME`: This is the full name and tag of the image built from the D3M_BASE_IMAGE. This name is chosen 
when the docker image is built by the user, for example the D3M_IMAGE_NAME of the following build directive `
docker build -f Dockerfile -t registry.datadrivendiscovery.org/ta2eval/ta2_performer:latest .`
is `registry.datadrivendiscovery.org/ta2eval/ta2_performer:latest`
* `D3M_IMAGE_DIGEST`: This is the digest of the image built from the D3M_BASE_IMAGE. This digest is only generated when 
the image is pushed to the repository. The digest is linked to the repository and not the image. If an image is built on 
the local machine, it will not have a digest when listed using `docker images --digests`. 

`D3M_IMAGE_NAME` and `D3M_IMAGE_DIGEST` can be the same as `D3M_BASE_IMAGE_NAME` and `D3M_BASE_IMAGE_DIGEST`
if the base D3M image was used directly.

When building your Docker image based on the base D3M image, you can also consider populating
`D3M_BASE_IMAGE_NAME` and `D3M_BASE_IMAGE_DIGEST` automatically during the build process,
baking them as constant environment variables into the Docker image. In your Dockerfile,
you can do something like:

```
ARG d3m_base_image_name
ENV D3M_BASE_IMAGE_NAME=$d3m_base_image_name

ARG d3m_base_image_digest
ENV D3M_BASE_IMAGE_DIGEST=$d3m_base_image_digest
```

Then pass arguments when building the Docker image:

```
export D3M_BASE_IMAGE="registry.gitlab.com/datadrivendiscovery/images/primitives:ubuntu-bionic-python36-v2020.5.18-20200612-000030"
docker build \
 --build-arg "d3m_base_image_name=$D3M_BASE_IMAGE" \
 --build-arg "d3m_base_image_digest=$(docker inspect --format='{{index .RepoDigests 0}}' "$D3M_BASE_IMAGE" | cut -d '@' -f 2)" \
 ...
```
